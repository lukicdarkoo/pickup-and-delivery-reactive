package types;


public class VData<T> {	
	public T action;
	public double value;
	
	public VData(T action, double value) {
		this.action = action;
		this.value = value;
	}
}
