package types;

import java.util.ArrayList;
import java.util.List;
import logist.topology.Topology.City;


public class Action_2N {
	public enum What { SKIP, PICK_AND_DELIVER }
	private City destination;
	private What what;
	
	private static ArrayList<Action_2N> actions = new ArrayList<Action_2N>();
	
	public static void init(List<City> cities) {
		for (City city : cities) {
			for (What reason : What.values()) {
				Action_2N.actions.add(new Action_2N(city, reason));
			}
		}
	}
	
	public Action_2N(City destination, What what) {
		this.destination = destination;
		this.what = what;
	}
	
	public static List<Action_2N> values() {
		return Action_2N.actions;
	}
	
	public What getWhat() {
		return what;
	}
	
	public City getDestination() {
		return destination;
	}
	
	public String toString() {
		return destination + "_" + what.name();
	}
}
