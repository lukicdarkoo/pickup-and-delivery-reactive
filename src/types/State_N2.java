package types;

import java.util.ArrayList;
import java.util.List;
import logist.topology.Topology.City;


/**
 * If `cityFrom` and `cityTo` than it means there is no mission.
 */
public class State_N2 {
	private City cityFrom;
	private City cityTo;
	
	private static ArrayList<State_N2> states = new ArrayList<State_N2>();
	public static void init(List<City> cities) {
		for (City cityFrom : cities) {
			for (City cityTo : cities) {
				if (cityFrom == cityTo) {
					State_N2.states.add(new State_N2(cityFrom, null));
				} else {
					State_N2.states.add(new State_N2(cityFrom, cityTo));
				}
			}
		}
	}
	
	public State_N2(City cityFrom, City cityTo) {
		this.cityFrom = cityFrom;
		this.cityTo = cityTo;
	}
	
	@Override
	public String toString() {
		return this.cityFrom.name + "_" + ((this.cityTo != null) ? this.cityTo.name : "None");
	}
	
	public City getCityFrom() {
		return this.cityFrom;
	}
	
	public City getCityTo() {
		return this.cityTo;
	}
	
	public boolean hasTask() {
		return this.cityTo != null;
	}
	
	public static State_N2 find(City cityFrom, City cityTo) {
		for (State_N2 state : states) {
			if (state.getCityFrom() == cityFrom && state.getCityTo() == cityTo) {
				return state;
			}
		}
		return null;
	}
	
	public static List<State_N2> values() {
		return State_N2.states;
	}
}
