package types;

import java.util.ArrayList;
import java.util.List;
import logist.topology.Topology.City;


public class State_N {
	private City city;
	
	private static ArrayList<State_N> states = new ArrayList<State_N>();
	public static void init(List<City> cities) {
		for (City city : cities) {
			State_N.states.add(new State_N(city));
		}
	}
	
	public State_N(City city) {
		this.city = city;
	}
	
	@Override
	public String toString() {
		return this.city.name;
	}
	
	public City getCity() {
		return this.city;
	}
	
	public static State_N find(City city) {
		for (State_N state : states) {
			if (state.getCity() == city) {
				return state;
			}
		}
		return null;
	}
	
	public static List<State_N> values() {
		return State_N.states;
	}
}
