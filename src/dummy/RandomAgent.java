package dummy;

import java.util.Random;

import logist.agent.Agent;
import logist.behavior.ReactiveBehavior;
import logist.simulation.Vehicle;
import logist.task.Task;
import logist.task.TaskDistribution;
import logist.topology.Topology;
import logist.topology.Topology.City;
import logist.plan.Action.Move;
import logist.plan.Action.Pickup;


public class RandomAgent implements ReactiveBehavior {
	private int numActions;
	private Agent agent;
    Random random;
	
	@Override
	public void setup(Topology topology, TaskDistribution distribution, Agent agent) {
		this.numActions = 0;
		this.agent = agent;
	    this.random = new Random();
	}
	
	@Override
	public logist.plan.Action act(Vehicle vehicle, Task availableTask) {
		logist.plan.Action action;
		
		if (availableTask == null || random.nextDouble() > 0.7) {
			City currentCity = vehicle.getCurrentCity();
			action = new Move(currentCity.randomNeighbor(random));
		} else {
			action = new Pickup(availableTask);
		}

		if (numActions >= 1) {
			System.out.println("The total profit after "+numActions+" actions is "+agent.getTotalProfit()+" (average profit: "+(agent.getTotalProfit() / (double)numActions)+")");
		}
		numActions++;

		return action;
	}
}

