package reactive;

import java.util.HashMap;
import java.util.Map;
import java.util.Random;
import logist.simulation.Vehicle;
import logist.agent.Agent;
import logist.behavior.ReactiveBehavior;
import logist.plan.Action.Move;
import logist.plan.Action.Pickup;
import logist.task.Task;
import logist.task.TaskDistribution;
import logist.topology.Topology;
import logist.topology.Topology.City;
import types.Action_2;
import types.State_N2;
import types.VData;


public class ReactiveAgent_N2_2 implements ReactiveBehavior {

	private Random random;
	private double discount;
	private int numActions;
	private Agent agent;
	private Topology topology;
	private TaskDistribution td;

	private Map<State_N2, VData<Action_2>> V = new HashMap<State_N2, VData<Action_2>>();

	@Override
	public void setup(Topology topology, TaskDistribution td, Agent agent) {
		Double discount = agent.readProperty("discount-factor", Double.class, 0.95);

		this.random = new Random();
		this.discount = discount;
		this.numActions = 0;

		this.agent = agent;
		this.td = td;
		this.topology = topology;

		doReinforcementLearning();
		printV();
	}

	private void doReinforcementLearning() {
		Map<State_N2, HashMap<Action_2, Double>> Q = new HashMap<State_N2, HashMap<Action_2, Double>>();

		State_N2.init(topology.cities());
		for (State_N2 state : State_N2.values()) {
			V.put(state, new VData<Action_2>(Action_2.values()[0], 0));
			Q.put(state, new HashMap<Action_2, Double>());
			for (Action_2 action : Action_2.values()) {
				Q.get(state).put(action, (double) 0);
			}
		}

		while (true) {
			Map<State_N2, VData<Action_2>> vPrev = new HashMap<State_N2, VData<Action_2>>(V);

			for (State_N2 state : State_N2.values()) {
				for (Action_2 action : Action_2.values()) {
					double qValue = getReward(state, action) + discount * getDiscountedSum(state, action);
					// System.out.println(state + " " +action + " " + getReward(state, action) + " " + getDiscountedSum(state, action));
					Q.get(state).put(action, qValue);
					// System.out.println(state + " " + action + " " + getDiscountedSum(state, action));
				}
				V.put(state, getMaxQ(Q.get(state)));
			}

			if (getMaxDiff(vPrev, V) < 0.001) break;
		}
	}

	private void printV() {
		System.out.println("V vector:");
		for (State_N2 state : V.keySet()) {
			VData<Action_2> vdata = V.get(state);
			System.out.println(state + " -> " + vdata.action + " (" + vdata.value + ")");
		}
	}

	private double getDiscountedSum(State_N2 state, Action_2 action) {
		double sum = 0.0;
		for (State_N2 statePrim : State_N2.values()) {
			sum += getProbability(state, action, statePrim) * V.get(statePrim).value;
		}
		return sum;
	}

	private double getNoTaskProbability(City city) {
		double prob = 0;
		for (City other : topology.cities()) {
			prob += td.probability(city, other);
		}
		return 1 - prob;
	}

	private double getAverageCostToNeighbor(City cityFrom) {
		double sum = 0;
		Vehicle v = agent.vehicles().iterator().next();

		for (City cityTo : cityFrom.neighbors()) {
			sum += cityFrom.distanceTo(cityTo) * v.costPerKm();
		}
		return sum / cityFrom.neighbors().size();
	}

	private double getReward(State_N2 state, Action_2 action) {
		Vehicle v = agent.vehicles().iterator().next();
		double cost = 0;

		if (state.hasTask()) {
			cost = state.getCityFrom().distanceTo(state.getCityTo()) * v.costPerKm();
		} else {
			cost = getAverageCostToNeighbor(state.getCityFrom());
		}

		if (action == Action_2.PICK && state.hasTask()) {
			return td.reward(state.getCityFrom(), state.getCityTo()) - cost;
		}
		return (- cost);
	}

	private double getProbability(State_N2 state, Action_2 action, State_N2 statePrim) {
		if (state.getCityTo() != statePrim.getCityFrom()) {
			return 0.0;
		}

		if (action == Action_2.PICK) {
			if (statePrim.hasTask()) {
				return td.probability(state.getCityTo(), statePrim.getCityTo());
			} else {
				return getNoTaskProbability(state.getCityTo());
			}
		} else {
			if (state.getCityTo().neighbors().contains(statePrim.getCityTo())) {
				return 1.0 / state.getCityTo().neighbors().size();
			} else {
				return 0.0;
			}
		}
	}

	private double getMaxDiff(Map<State_N2, VData<Action_2>> vPrev, Map<State_N2, VData<Action_2>> vCurr) {
		double maxDiff = 0.0;
		for (State_N2 state : vPrev.keySet()) {
			double diff = Math.abs(vPrev.get(state).value - vCurr.get(state).value);
			if (diff > maxDiff) {
				maxDiff = diff;
			}
		}
		return maxDiff;
	}

	private VData<Action_2> getMaxQ(Map<Action_2, Double> actions) {
		VData<Action_2> maxVData = null;
		for (Action_2 action : actions.keySet()) {
			double value = actions.get(action);
			if (maxVData == null) {
				maxVData = new VData<Action_2>(action, value);
			}
			if (value > maxVData.value) {
				maxVData.action = action;
				maxVData.value = value;
			}
		}
		return maxVData;
	}

	@Override
	public logist.plan.Action act(Vehicle vehicle, Task availableTask) {
		logist.plan.Action action;
		City city = vehicle.getCurrentCity();
		State_N2 state = State_N2.find(city, availableTask == null ? null : availableTask.deliveryCity);
		Action_2 plannedAction = V.get(state).action;

		if (plannedAction == Action_2.SKIP || !state.hasTask()) {
			action = new Move(city.randomNeighbor(random));
			System.out.println(availableTask + " Move Empty");
		}
		else {
			action = new Pickup(availableTask);
			System.out.println("Pick up");
		}

		if (numActions >= 1) {
			System.out.println("The total profit after "+numActions+" actions is "+agent.getTotalProfit()+" (average profit: "+(agent.getTotalProfit() / (double)numActions)+")");
		}
		numActions++;

		return action;
	}
}

