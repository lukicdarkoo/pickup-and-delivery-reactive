package reactive;

import java.util.HashMap;
import java.util.Map;
import java.util.Random;
import logist.simulation.Vehicle;
import logist.agent.Agent;
import logist.behavior.ReactiveBehavior;
import logist.plan.Action.Move;
import logist.plan.Action.Pickup;
import logist.task.Task;
import logist.task.TaskDistribution;
import logist.topology.Topology;
import logist.topology.Topology.City;
import types.Action_2;
import types.State_N;
import types.VData;


/**
 * The solution is based on N states and 2 actions, where N is number of cities.
 * Possible states are "<CITY>" and possible actions are "<SKIP|PICK>".
 */
public class ReactiveAgent_N_2 implements ReactiveBehavior {
	
	private Random random;
	private double discount;
	private int numActions;
	private Agent agent;
	private Topology topology;
	private TaskDistribution td;
	
	private Map<State_N, VData<Action_2>> V = new HashMap<State_N, VData<Action_2>>();

	@Override
	public void setup(Topology topology, TaskDistribution td, Agent agent) {
		Double discount = agent.readProperty("discount-factor", Double.class, 0.95);
		
		this.random = new Random();
		this.discount = discount;
		this.numActions = 0;
		
		this.agent = agent;
		this.td = td;
		this.topology = topology;

		doReinforcementLearning();
		printV();
	}
	
	private void doReinforcementLearning() {
		Map<State_N, HashMap<Action_2, Double>> Q = new HashMap<State_N, HashMap<Action_2, Double>>();

		State_N.init(topology.cities());
		for (State_N state : State_N.values()) {
			V.put(state, new VData<Action_2>(Action_2.values()[0], random.nextDouble() * 1000));
			Q.put(state, new HashMap<Action_2, Double>());
			for (Action_2 action : Action_2.values()) {
				Q.get(state).put(action, (double) 0);
			}
		}
		
		while (true) {
			Map<State_N, VData<Action_2>> vPrev = new HashMap<State_N, VData<Action_2>>(V);
			
			for (State_N state : State_N.values()) {
				for (Action_2 action : Action_2.values()) {
					double qValue = getReward(state, action) + discount * getDiscountedSum(state, action);
					Q.get(state).put(action, qValue);
 				}
				V.put(state, getMaxQ(Q.get(state)));
			}
			
			if (getMaxDiff(vPrev, V) < 0.001) break;
		}
	}
	
	private void printV() {
		System.out.println("V vector:");
		for (State_N state : V.keySet()) {
			VData<Action_2> vdata = V.get(state);
			System.out.println(state + " -> " + vdata.action + " (" + vdata.value + ")");
		}
	}
	
	private double getDiscountedSum(State_N state, Action_2 action) {
		double sum = 0.0;
		for (State_N statePrim : State_N.values()) {
			sum += getProbability(state, action, statePrim) * V.get(statePrim).value;
		}
		return sum;
	}
	
	private double getProbability(State_N state, Action_2 action, State_N statePrim) {
		if (action == Action_2.PICK) {
			return td.probability(state.getCity(), statePrim.getCity());
		} else {
			if (state.getCity().neighbors().contains(statePrim.getCity())) {
				return 1.0 / state.getCity().neighbors().size();
			} else {
				return 0.0;
			}
		}
	}
	
	private double getMaxDiff(Map<State_N, VData<Action_2>> vPrev, Map<State_N, VData<Action_2>> vCurr) {
		double maxDiff = 0.0;
		for (State_N state : vPrev.keySet()) {
			double diff = Math.abs(vPrev.get(state).value - vCurr.get(state).value);
			if (diff > maxDiff) {
				maxDiff = diff;
			}
		}
		return maxDiff;
	}
	
	private VData<Action_2> getMaxQ(Map<Action_2, Double> actions) {
		VData<Action_2> maxVData = null;
		for (Action_2 action : actions.keySet()) {
			double value = actions.get(action);
			if (maxVData == null) {
				maxVData = new VData<Action_2>(action, value);
			}
			if (value > maxVData.value) {
				maxVData.action = action;
				maxVData.value = value;
			}
		}
		return maxVData;
	}
	
	private double getReward(State_N state, Action_2 action) {
		double cost = getAverageCostToNeighbors(state.getCity());
		
		if (action == Action_2.PICK) {
			return getAverageRewardFrom(state.getCity()) - cost;
		}
		return (- cost);
	}

	private double getAverageRewardFrom(City cityFrom) {
		double sum = 0;
		int count = 0;
		for (City cityTo : topology.cities()) {
			if (cityFrom != cityTo) {
				sum += td.reward(cityFrom, cityTo);
				count++;
			}
		}
		return sum / count;
	}

	private double getAverageCostToNeighbors(City cityFrom) {
		double sum = 0;
		Vehicle v = agent.vehicles().iterator().next();
		
		for (City cityTo : cityFrom.neighbors()) {
			if (cityFrom != cityTo) {
				sum += cityFrom.distanceTo(cityTo) * v.costPerKm();
			}
		}
		return sum / (topology.cities().size() - 1);
	}


	@Override
	public logist.plan.Action act(Vehicle vehicle, Task availableTask) {
		logist.plan.Action action;
		City city = vehicle.getCurrentCity();
		State_N state = State_N.find(city);
		Action_2 planedAction = V.get(state).action;
		
		if (planedAction == Action_2.SKIP) {
			action = new Move(city.randomNeighbor(random));
			System.out.println("Move empty");
		} else {
			if (availableTask != null) {
				action = new Pickup(availableTask);
				System.out.println("Pick up");
			} else {
				action = new Move(city.randomNeighbor(random));
				System.out.println("No tasks, move empty");
			}
		}

		if (numActions >= 1) {
			System.out.println("The total profit after "+numActions+" actions is "+agent.getTotalProfit()+" (average profit: "+(agent.getTotalProfit() / (double)numActions)+")");
		}
		numActions++;

		return action;
	}
}

