package reactive;

import java.util.HashMap;
import java.util.Map;
import java.util.Random;
import logist.simulation.Vehicle;
import logist.agent.Agent;
import logist.behavior.ReactiveBehavior;
import logist.plan.Action.Move;
import logist.plan.Action.Pickup;
import logist.task.Task;
import logist.task.TaskDistribution;
import logist.topology.Topology;
import logist.topology.Topology.City;
import types.Action_2N;
import types.State_N;
import types.VData;


/**
 * The solution is based on N states and 2*N actions, where N is number of cities.
 * Possible states are "<CITY>" and possible actions are "<CITY>_<SKIP|PICK>".
 * The agents tend to PICK_AND_DELIVERY every time, even with different environment configurations.
 */
public class ReactiveAgent_N_2N implements ReactiveBehavior {
	
	private Random random;
	private double discount;
	private int numActions;
	private Agent agent;
	private Topology topology;
	private TaskDistribution td;
	
	private Map<State_N, VData<Action_2N>> V = new HashMap<State_N, VData<Action_2N>>();

	@Override
	public void setup(Topology topology, TaskDistribution td, Agent agent) {
		Double discount = agent.readProperty("discount-factor", Double.class, 0.95);
		
		this.random = new Random();
		this.discount = discount;
		this.numActions = 0;
		
		this.agent = agent;
		this.td = td;
		this.topology = topology;

		doReinforcementLearning();
		printV();
	}
	
	private void doReinforcementLearning() {
		Map<State_N, HashMap<Action_2N, Double>> Q = new HashMap<State_N, HashMap<Action_2N, Double>>();

		State_N.init(topology.cities());
		Action_2N.init(topology.cities());
		for (State_N state : State_N.values()) {
			V.put(state, new VData<Action_2N>(Action_2N.values().get(0), 0));
			Q.put(state, new HashMap<Action_2N, Double>());
			for (Action_2N action : Action_2N.values()) {
				Q.get(state).put(action, 0.0);
			}
		}
		
		while (true) {
			Map<State_N, VData<Action_2N>> vPrev = new HashMap<State_N, VData<Action_2N>>(V);
			
			for (State_N state : State_N.values()) {
				for (Action_2N action : Action_2N.values()) {
					double qValue = getReward(state, action) + discount * getDiscountedSum(state, action);
					Q.get(state).put(action, qValue);
					//System.out.println(state + " + " + action + " = " + qValue);
					System.out.println(getReward(state, action));
 				}
				V.put(state, getMaxQ(Q.get(state)));
			}
			if (getMaxDiff(vPrev, V) < 1) break;
		}
	}
	
	private void printV() {
		System.out.println("V vector:");
		for (State_N state : V.keySet()) {
			VData<Action_2N> vdata = V.get(state);
			System.out.println(state + " -> " + vdata.action + " (" + vdata.value + ")");
		}
	}
	
	private double getMaxDiff(Map<State_N, VData<Action_2N>> vPrev, Map<State_N, VData<Action_2N>> vCurr) {
		double maxDiff = 0.0;
		for (State_N state : vPrev.keySet()) {
			double diff = Math.abs(vPrev.get(state).value - vCurr.get(state).value);
			if (diff > maxDiff) {
				maxDiff = diff;
			}
		}
		return maxDiff;
	}
	
	private VData<Action_2N> getMaxQ(Map<Action_2N, Double> actions) {
		VData<Action_2N> maxVData = null;
		for (Action_2N action : actions.keySet()) {
			double value = actions.get(action);
			if (maxVData == null) {
				maxVData = new VData<Action_2N>(action, value);
			}
			if (value > maxVData.value) {
				maxVData.action = action;
				maxVData.value = value;
			}
		}
		return maxVData;
	}
	
	private double getDiscountedSum(State_N state, Action_2N action) {
		double sum = 0.0;
		for (State_N statePrim : State_N.values()) {
			sum += getProbability(state, action, statePrim) * V.get(statePrim).value;
		}
		return sum;
	}
	
	private double getProbability(State_N state, Action_2N action, State_N statePrim) {
		if (action.getDestination() == statePrim.getCity()) {
			if (action.getWhat() == Action_2N.What.PICK_AND_DELIVER) {
				return td.probability(state.getCity(), action.getDestination());
			} else if (state.getCity().neighbors().contains(action.getDestination())) {
				return 1.0;
			}
		}
		return 0.0;
	}
	
	private double getReward(State_N state, Action_2N action) {
		Vehicle v = agent.vehicles().iterator().next();
		double cost = state.getCity().distanceTo(action.getDestination()) * v.costPerKm();
		
		if (action.getWhat() == Action_2N.What.PICK_AND_DELIVER) {
			return td.reward(state.getCity(), action.getDestination()) - cost;
		}
		return (- cost);
	}


	@Override
	public logist.plan.Action act(Vehicle vehicle, Task availableTask) {
		logist.plan.Action action;
		City city = vehicle.getCurrentCity();
		State_N state = State_N.find(city);
		Action_2N planedAction = V.get(state).action;
		
		if (planedAction.getWhat() == Action_2N.What.SKIP) {
			action = new Move(planedAction.getDestination());
			System.out.println("Move empty");
		} else {
			if (availableTask != null) {
				action = new Pickup(availableTask);
				System.out.println("Pick up");
			} else {
				action = new Move(city.randomNeighbor(random));
				System.out.println("No tasks, move empty");
			}
		}

		if (numActions >= 1) {
			System.out.println("The total profit after "+numActions+" actions is "+agent.getTotalProfit()+" (average profit: "+(agent.getTotalProfit() / (double)numActions)+")");
		}
		numActions++;

		return action;
	}
}

