# Pickup-and-Delivery: Reactive

## Running
Run the following code to make **bulky** and **bulky-disc** agents compete:
```
java -jar logist/logist.jar config/reactive.xml bulky bulky-disc
```

## Assigment
https://moodle.epfl.ch/mod/assign/view.php?id=835963