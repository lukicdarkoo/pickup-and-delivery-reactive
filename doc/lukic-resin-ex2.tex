\documentclass[11pt]{article}

\usepackage{amsmath}
\usepackage{textcomp}
\usepackage[top=0.8in, bottom=0.8in, left=0.8in, right=0.8in]{geometry}
\usepackage{graphicx}
\usepackage{subcaption}
\graphicspath{ {../assets/} }

% put your group number and names in the author field
\title{\bf Exercise 2: A Reactive Agent for the Pickup and Delivery Problem}
\author{Group \textnumero 34: David Resin, Darko Lukic}

% the report should not be longer than 3 pages

\begin{document}
\maketitle

\section{Problem Representation}
The problem and initial assumptions are given in the exercises "A Reactive Agent for the Pickup and Delivery Problem".

\subsection{Representation Description}
A few iterations of the agent are developed. The goal of each iteration is to find compromise between simplicity and efficiency. The simplification is achieved by reducing number of states and/or actions and approximation.
The latest iteration is based on $ N^{2} $ states and 2 actions where N is a number of cities.

\subsubsection{Experimental Reactive Agent \#1}
The first iteration of an agent has N states and 2 actions where N is a number of cities. 
Therefore, states are $ s_{city\_name} $ and actions are $ a_{SKIP} $ and $ a_{PICK} $. 
The reward table is calculated as:

\begin{equation} 
    \label{eq:1}
    R(s, a) = 
    \begin{cases}
        - avg\_cost\_to\_neighbors(s_{city}), & a = a_{SKIP} \\
        avg\_reward\_from(a_{city}) - avg\_cost\_to\_neighbors(a_{city}), & a = a_{PICK}
    \end{cases}    
\end{equation}

and the probability table as:
\begin{equation}
    \label{eq:2}
    T(s, a, s') = 
    \begin{cases}
        - avg\_probability(s_{city}, s'_{city}), & a = a_{PICK} \\
        1 / n\_of\_neighbors(s_{city}), & a = a_{SKIP} \quad \textrm{and} \quad is\_neighbor(from, to) \\
        0, & is\_not\_neighbor(s_{city}, s'_{city})
    \end{cases}  
\end{equation}
% state representation, 
% the possible actions, 
% the reward table and 
% the probability transition table

\subsubsection{Final Reactive Agent}
After a few iterations the final agent is designed. It gives targeted compromise
between simplicity (robustness) and efficiency. The solution is based on $N^{2}$ states and 
2 actions. Therefore, the states are $ s_{from\_to} $, where
$ s_{city_city} $ (from = to) means \textit{"no task found"}. Actions are $a_{SKIP}$ and $a_{PICK}$.

The reward table is formulated as:
\begin{equation} 
    \label{eq:3}
    R(s, a) = 
    \begin{cases}
        reward(s_{from}, s_{to}) - transport\_cost(s_{from}, s_{to}), & a = a_{PICK} \quad \textrm{and} \quad s_{has\_task} \\
        - avg\_cost\_to\_neighbor(s_{from}), & a = a_{SKIP} \quad \textrm{or} \quad s_{no\_task}
    \end{cases}    
\end{equation}
In the the function (Equation \ref{eq:3}) the cost of transportation to another city is given as 
an average as the agent is not designed to choose the neighbor city (action can only be $a_{PICK}$ or $a_{SKIP}$). 
And the probability table:

\begin{equation}
    \label{eq:4}
    T(s, a, s') = 
    \begin{cases}
        probability(s_{to}, s'_{to}), & a = a_{PICK} \quad \textrm{and} \quad s_{has\_task} \\
        no\_task\_probability(s_{to}), & a = a_{PICK} \quad \textrm{and} \quad s_{no\_task} \\
        1 / n\_of\_neighbors(s_{to}), & a = a_{SKIP} \quad \textrm{and} \quad is\_neighbor(s_{to}, s'_{to}) \\
        0, & is\_not\_neighbor(s_{to}, s'_{to})
    \end{cases}  
\end{equation}



\subsection{Implementation Details}
% representations above and the implementation details of the reinforcement learning algorithm you implemented
The implementation of algorithm is based on the given algorithm (\textit{"A Reactive Agent for the Pickup and Delivery Problem"}, p.2).
Both vectors, Q and V, are represented as hash maps and initialised with 0. Reward and probability tables are implemented
as methods. 

\textit{.getMaxDiff()} checks if the algorithm converged by comparing max difference between values in $V_{i-1}$ and $V_{i}$ tables. If
the max difference is less then n (where n is very small number) the algorithm will finish.

Also, a few classes are developed to give support in prototyping different type of reactive agents. Using the classes
it is easy to generate different number of actions and states. A package \textbf{reactive} contains
3 different implementation of reactive agents, so the user can do a comparison.

\section{Results}
% in this section, you describe several results from the experiments with your reactive agent

\subsection{Experiment 1: Discount factor}

The purpose of the first experiment is to notice an influence of discount factor on efficiency of agents.
The expected result of the experiment is get higher efficiency with higher value of discount factor.

\subsubsection{Setting}
A command used for conducting the experiment:

\textit{java -jar logist/logist.jar config/reactive.xml bulky bulky-disc}

\noindent
The following parameters are used for \textbf{bulky} and  \textbf{bulky-disc}:

\begin{table}[h]
    \begin{tabular}{l | l l l}
                            & \textbf{discount factor} & \textbf{class}                & \textbf{cost per km} \\
        \hline
        \textbf{bulky}      & 0.85                     & reactive.ReactiveAgent\_N2\_2 & 5                    \\
        \textbf{bulky-disc} & 0.15                     & reactive.ReactiveAgent\_N2\_2 & 5                   
    \end{tabular}
    \label{tab:1}
    \caption{Configuration parameters for experiment 1}
\end{table}

\subsubsection{Observations}

As it is noticeable in the plot (Figure \ref{fig:plot1}) the expected result is achieved.

\begin{figure}[h]
    \centering
    \includegraphics[width=.53\textwidth]{bulky_15_85.png}
    \centering
    \caption{Comparison of different values for discount factor}
    \label{fig:plot1}
\end{figure}


\subsection{Experiment 2: Comparisons with dummy agents}
In this experiment efficiency of the reactive agent is compared to a dummy agent.
It is expected that reactive agent outperform dummy agents.

\subsubsection{Setting}
A command used for conducting the experiment:

\textit{java -jar logist/logist.jar config/reactive.xml bulky random}

\noindent
The following parameters are used for \textbf{bulky} and  \textbf{random}:

\begin{table}[h]
    \begin{tabular}{l | l l l}
                            & \textbf{discount factor} & \textbf{class}                & \textbf{cost per km} \\
        \hline
        \textbf{bulky}      & 0.85                     & reactive.ReactiveAgent\_N2\_2 & 5                    \\
        \textbf{bulky-disc} & -                     & dummy.RandomAgent & 5                   
    \end{tabular}
    \label{tab:2}
    \caption{Configuration parameters for experiment 2}
\end{table}


\subsubsection{Observations}

As expected, the reactive agent is far more efficient than the dummy agent (Figure \ref{fig:plot2}).

\begin{figure}[h]
    \centering
    \includegraphics[width=.53\textwidth]{bulky_85_vs_dummy.png}
    \centering
    \caption{Comparison of different values for discount factor}
    \label{fig:plot2}
\end{figure}

\end{document}